package com.devcamp.task76_50.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task76_50.entity.Office;
import com.devcamp.task76_50.repository.IOfficeRepository;

@CrossOrigin
@RestController
@RequestMapping("/offices")
public class OfficeController {
    @Autowired
    private IOfficeRepository pOfficeRepository;

    @GetMapping
    public ResponseEntity<List<Office>> getAllOffices() {
        try {
            List<Office> offices = pOfficeRepository.findAll();
            return new ResponseEntity<>(offices, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Office> getOfficeById(@PathVariable("id") int id) {
        Optional<Office> officeData = pOfficeRepository.findById(id);
        if (officeData.isPresent()) {
            return new ResponseEntity<>(officeData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<Office> createOffice(@Valid @RequestBody Office office) {
        try {
            Office createdOffice = pOfficeRepository.save(office);
            return new ResponseEntity<>(createdOffice, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Office> updateOfficeById(@PathVariable("id") int id,
            @Valid @RequestBody Office updatedOffice) {
        Optional<Office> officeData = pOfficeRepository.findById(id);
        if (officeData.isPresent()) {
            Office office = officeData.get();
            office.setCity(updatedOffice.getCity());
            office.setPhone(updatedOffice.getPhone());
            office.setAddressLine(updatedOffice.getAddressLine());
            office.setState(updatedOffice.getState());
            office.setCountry(updatedOffice.getCountry());
            office.setTerritory(updatedOffice.getTerritory());

            try {
                Office updatedOfficeData = pOfficeRepository.save(office);
                return new ResponseEntity<>(updatedOfficeData, HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteOfficeById(@PathVariable("id") int id) {
        try {
            pOfficeRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
